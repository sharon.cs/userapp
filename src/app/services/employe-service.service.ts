import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmployeServiceService {

  constructor(private http:HttpClient) { }
  registerEmployeDetails(employeEntry:any){
    return this.http.post(environment.apiurl+"/auth/register",employeEntry);
  }
  loginEmploye(employeAuthent:any){
    return this.http.post(environment.apiurl+"/auth/login",employeAuthent);
  }
  checkExistingEmail(employeEmail:any){
    return this.http.post(environment.apiurl+"/auth/checkMail",employeEmail);
  }
}
