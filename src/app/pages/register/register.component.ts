import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmployeServiceService } from 'src/app/services/employe-service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registrationForm!:FormGroup;
  emailExisting = false;
  passwordMissMatch=false;
  constructor(private fb:FormBuilder,private _employeervice:EmployeServiceService) { }

  ngOnInit(): void {
    this.registrationForm=this.fb.group({
      email:['',[Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
      password:['',[Validators.required]],
      username:['',[Validators.required]],
      phone:['',[Validators.required,Validators.pattern("[0-9]{10}")]],
      Conformpassword:['',[Validators.required]]
    });
  }
  registerEmploye(){
    this._employeervice.registerEmployeDetails(this.registrationForm.value)
      .subscribe(data=> {console.log(data)});
  }
  checkEmail(){
    var EmailObj={'email':this.registrationForm.get('email')?.value}
   //console.log(EmailObj);
   this._employeervice.checkExistingEmail(EmailObj)
      .subscribe((data:any)=>{
        if(data.message=="Email already exist"){
          this.emailExisting=true;
        }
        else{
          this.emailExisting=false;
        }
        console.log(data)});
  }
  passwordCheck(){
    if(this.registrationForm.get('password')?.value==this.registrationForm.get('Conformpassword')?.value){
      this.passwordMissMatch=false;
    }
    else{
      this.passwordMissMatch=true;
    }
  }
}
