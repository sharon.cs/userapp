import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmployeServiceService } from 'src/app/services/employe-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm!:FormGroup;
  constructor(private fb:FormBuilder,private _employeervice:EmployeServiceService) { }

  ngOnInit(): void {
    this.loginForm=this.fb.group(
      {
        email:['',[Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
        password:['',[Validators.required]]
      }
    );
  }
  login(){
    this._employeervice.loginEmploye(this.loginForm.value)
      .subscribe((data:any)=>{console.log(data)
        if(data.success){
          alert("Welcome To Your profile");
        }
      });
  }
}
